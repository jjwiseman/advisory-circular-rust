/// Tools for using the airport-data.com API.
///
/// airport-data.com is the only source I know of for aircraft photos that we
/// can legally tweet.
use std::time::Instant;

use log::debug;
use reqwest::Url;
use serde::Deserialize;

use crate::{errors::Error, USER_AGENT};

#[derive(Deserialize, Debug, PartialEq, Clone)]
pub struct AirportDataResponse {
    pub data: Option<Vec<AirportData>>,
}

#[derive(Deserialize, Debug, PartialEq, Clone)]
pub struct AirportData {
    pub image: String,
    pub link: String,
    pub photographer: String,
}

/// Get a photo of an aircraft from airport-data.com.
pub async fn get_photo(icao: &str) -> Result<Option<Vec<u8>>, Error> {
    let start = Instant::now();
    let url = Url::parse_with_params(
        "https://www.airport-data.com/api/ac_thumb.json",
        &[("m", icao), ("n", "1")],
    )
    .map_err(|e| {
        Error::AircraftPhotoError(format!(
            "Error parsing url for aircraft photo &m={}&n={}: {}",
            &icao, "1", e
        ))
    })?;
    debug!("{} Fetching aircraft photos from {}", icao, url);
    let client = reqwest::Client::builder()
        .user_agent(USER_AGENT)
        .gzip(true)
        .build()
        .map_err(|e| {
            Error::AircraftPhotoError(format!("Error building airport-data.com client: {}", e))
        })?;
    let resp = client.get(url.clone()).send().await.map_err(|e| {
        Error::AircraftPhotoError(format!(
            "Error making airport-data API request to {}: {}",
            &url, e
        ))
    })?;
    let resp = resp.error_for_status().map_err(|e| {
        Error::AircraftPhotoError(format!(
            "airport-data API returned an error status for {}: {}",
            &url, e
        ))
    })?;
    let body = resp.text().await.map_err(|e| {
        Error::AircraftPhotoError(format!("Error reading airport-data API response: {}", e))
    })?;
    let data = serde_json::from_str::<AirportDataResponse>(&body).map_err(|e| {
        Error::AircraftPhotoError(format!("Error parsing airport-data API response: {}", e))
    })?;
    match data.data {
        Some(data) => {
            if !data.is_empty() {
                let resp = client.get(&data[0].image).send().await.map_err(|e| {
                    Error::AircraftPhotoError(format!(
                        "Error making airport-data API request to {}: {}",
                        &data[0].image, e
                    ))
                })?;
                let resp = resp.error_for_status().map_err(|e| {
                    Error::AircraftPhotoError(format!(
                        "airport-data API returned an error status for {}: {}",
                        &data[0].image, e
                    ))
                })?;
                let body = resp.bytes().await.map_err(|e| {
                    Error::AircraftPhotoError(format!(
                        "Error reading airport-data API response: {}",
                        e
                    ))
                })?;
                let elapsed = start.elapsed();
                debug!(
                    "{} Fetched aircraft photo in {}.{:03}s",
                    icao,
                    elapsed.as_secs(),
                    elapsed.subsec_millis()
                );
                Ok(Some(body.to_vec()))
            } else {
                debug!("{} No aircraft photos found", icao);
                Ok(None)
            }
        }
        None => Ok(None),
    }
}

#[cfg(test)]
mod tests {
    #![allow(clippy::unwrap_used)]

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[tokio::test]
    async fn test_get_photo() {
        // Yes this requires a network connection and also for the
        // airport-data.com API to be up.
        let actual = get_photo("A092FE").await.unwrap().unwrap();
        // let mut file = std::fs::File::create("test.jpg").unwrap();
        // std::io::Write::write_all(&mut file, &actual).unwrap();
        assert_eq!(&actual[6..10], "JFIF".as_bytes());
    }
}
