use std::{str::FromStr, time::Instant};

use adsbx_json::v2::Response;
use adsbx_screenshot::{AdsbxBrowser, AdsbxBrowserOptions, Screenshot};
use log::debug;
use reqwest::{header::HeaderMap, Url};

use crate::{config::AppConfig, errors::Error};

/// The ADS-B Exchange (ADSBX) API client.
pub struct AdsbxApi<'a> {
    config: &'a AppConfig,
}

impl<'a> AdsbxApi<'a> {
    /// Creates an `AdsbxApi` instance from the app config.
    pub fn from_config(config: &'a AppConfig) -> AdsbxApi<'a> {
        AdsbxApi { config }
    }

    /// Returns aircraft within some distance of a location.
    pub async fn get_nearby(&self, lat: f64, lon: f64, radius_nm: f64) -> Result<Response, Error> {
        let start = Instant::now();
        let url = self.url_for_position(lat, lon, radius_nm)?;
        debug!("Fetching ADSBX data from {}", &url);
        let headers = self.headers();
        let client = reqwest::Client::builder()
            .user_agent("advisorycircular / 0.0")
            .gzip(true)
            .default_headers(headers)
            .build()
            .map_err(|e| {
                Error::AdsbxApiError(format!("Error creating http client for adsbx api: {}", e))
            })?;
        let resp = client.get(&url).send().await.map_err(|e| {
            Error::AdsbxApiError(format!("Error fetching adsbx data from {}: {}", &url, e))
        })?;
        let resp = resp.error_for_status().map_err(|e| {
            Error::AdsbxApiError(format!("Error fetching adsbx data from {}: {}", &url, e))
        })?;
        let body = resp.text().await.map_err(|e| {
            Error::AdsbxApiError(format!("Error fetching adsbx data from {}: {}", &url, e))
        })?;
        let response = Response::from_str(&body).map_err(|e| {
            Error::AdsbxApiError(format!("Error parsing adsbx data from {}: {}", &url, e))
        })?;
        debug!(
            "Fetching ADSBX data for {} aircraft took {} ms",
            response.num_aircraft,
            start.elapsed().as_millis()
        );
        Ok(response)
    }

    /// Returns all aircraft currently being tracked by ADSBX anywhere in the
    /// world.
    pub async fn get_all(&self) -> Result<Response, Error> {
        let start = Instant::now();
        let url = self.url_for_all()?;
        debug!("Fetching ADSBX data from {}", &url);
        let headers = self.headers();
        let client = reqwest::Client::builder()
            .user_agent("adsbx_json_fetch / 0.0")
            .gzip(true)
            .default_headers(headers)
            .build()
            .map_err(|e| {
                Error::AdsbxApiError(format!("Error creating http client for adsbx api: {}", e))
            })?;
        let resp = client.get(&url).send().await.map_err(|e| {
            Error::AdsbxApiError(format!("Error fetching adsbx data from {}: {}", &url, e))
        })?;
        let resp = resp.error_for_status().map_err(|e| {
            Error::AdsbxApiError(format!("Error fetching adsbx data from {}: {}", &url, e))
        })?;
        let body = resp.text().await.map_err(|e| {
            Error::AdsbxApiError(format!("Error fetching adsbx data from {}: {}", &url, e))
        })?;
        let response = Response::from_str(&body).map_err(|e| {
            Error::AdsbxApiError(format!("Error parsing adsbx data from {}: {}", &url, e))
        })?;
        debug!(
            "Fetching ADSBX data for {} aircraft took {} ms",
            response.num_aircraft,
            start.elapsed().as_millis()
        );
        Ok(response)
    }

    fn url_for_position(&self, lat: f64, lon: f64, radius_nm: f64) -> Result<String, Error> {
        let url: Url = self.config.adsbx.url.parse().map_err(|e| {
            Error::ConfigError(format!(
                "Error parsing adsbx url {}: {}",
                self.config.adsbx.url, e
            ))
        })?;
        let path = format!("lat/{}/lon/{}/dist/{:.0}", lat, lon, radius_nm.ceil());
        Ok(url
            .join(&path)
            .map_err(|e| {
                Error::AdsbxApiError(format!("Error building ADSBX API 'nearby' url: {}", e))
            })?
            .to_string())
    }

    fn url_for_all(&self) -> Result<String, Error> {
        let url: Url = self.config.adsbx.url.parse().map_err(|e| {
            Error::ConfigError(format!(
                "Error parsing adsbx url {}: {}",
                self.config.adsbx.url, e
            ))
        })?;
        let path = "all";
        Ok(url
            .join(path)
            .map_err(|e| {
                Error::AdsbxApiError(format!("Error building ADSBX API 'all' url: {}", e))
            })?
            .to_string())
    }

    fn headers(&self) -> HeaderMap {
        let mut headers = HeaderMap::new();
        let creds = self
            .config
            .adsbx
            .creds
            .as_ref()
            .expect("ADSBX credentials must be specified in secrets file");
        headers.insert(
            "api-auth",
            creds
                .api_key
                .parse()
                .expect("Invalid characters in ADSBX API key"),
        );
        if let Some(whitelist) = &creds.api_whitelist {
            headers.insert(
                "ADSBX-WL",
                whitelist
                    .parse()
                    .expect("Invalid characters in ADSBX whitelist key"),
            );
        }
        headers
    }
}

/// Gets a screenshot via the ADSBX website.
pub fn screenshot(icao: &str, config: &AppConfig) -> Result<Screenshot, Error> {
    let default_dimensions: (i32, i32) = (800, 600);
    let dimensions = &config
        .screenshot
        .as_ref()
        .and_then(|s| s.dimensions)
        .unwrap_or(default_dimensions);
    let mut browser = AdsbxBrowser::new(*dimensions).map_err(|e| {
        Error::AdsbxScreenshotError(format!("Error preparing to take ADSBX screenshot: {}", e))
    })?;
    let s_config = screenshot_config(icao, config);
    browser
        .screenshot(&s_config)
        .map_err(|e| Error::AdsbxScreenshotError(format!("Error taking ADSBX screenshot: {}", e)))
}

// Generates an ADSBXBrowserOptions to take a screenshot of the given ICAO using
// the app config.
fn screenshot_config(icao: &str, config: &AppConfig) -> AdsbxBrowserOptions {
    let layer = config
        .screenshot
        .as_ref()
        .and_then(|s| s.layer.as_ref().map(|l| l.to_string()));
    AdsbxBrowserOptions {
        icaos: vec![icao.to_string()],
        // show_labels: true,
        show_track_labels: false,
        delete_ads: true,
        layer,
        ..AdsbxBrowserOptions::default()
    }
}
