use rusqlite::Connection;

use crate::errors::Error;

#[derive(Debug, PartialEq, PartialOrd, Hash, Clone)]
pub struct Record {
    pub registration: Option<String>,
    pub ac_type: Option<String>,
}

pub fn get_record(icao: &str, db_path: &str) -> Result<Option<Record>, Error> {
    let db = Connection::open(db_path).map_err(|e| {
        Error::AircraftDbError(format!(
            "Error opening aircraft database at {}: {}",
            db_path, e
        ))
    })?;
    match db.query_row(
        "SELECT registration, type FROM aircraft WHERE icao = ?1",
        &[&icao.to_uppercase()],
        |row| {
            let mut registration: Option<String> = row.get(0)?;
            match registration {
                Some(v) if v.is_empty() => registration = None,
                _ => {}
            }
            let mut ac_type: Option<String> = row.get(1)?;
            match ac_type {
                Some(v) if v.is_empty() => ac_type = None,
                _ => {}
            }
            Ok(Record {
                registration,
                ac_type,
            })
        },
    ) {
        Ok(r) => Ok(Some(r)),
        Err(rusqlite::Error::QueryReturnedNoRows) => Ok(None),
        Err(e) => Err(Error::AircraftDbError(format!(
            "Error querying aircraft database at {}: {}",
            db_path, e
        ))),
    }
}

pub fn create_db_from_json_file(json_path: &str, db_path: &str) -> Result<(), Error> {
    let db = Connection::open(db_path).map_err(|e| {
        Error::AircraftDbError(format!(
            "Error opening aircraft database at {}: {}",
            db_path, e
        ))
    })?;
    db.execute_batch(
        "DROP TABLE IF EXISTS aircraft;
             DROP INDEX IF EXISTS idx_aircraft_icao;
             CREATE TABLE aircraft (
               icao TEXT NOT NULL PRIMARY KEY,
               registration TEXT,
               type TEXT
             );
             BEGIN TRANSACTION;",
    )
    .map_err(|e| {
        Error::AircraftDbError(format!(
            "Error creating aircraft database {}: {}",
            db_path, e
        ))
    })?;
    let json_data = std::fs::read_to_string(json_path).map_err(|e| {
        Error::AircraftDbError(format!(
            "Error reading mictronics json file {}: {}",
            json_path, e
        ))
    })?;
    let json_data: serde_json::Value = serde_json::from_str(&json_data).map_err(|e| {
        Error::AircraftDbError(format!(
            "Error parsing mictronics json file {}: {}",
            json_path, e
        ))
    })?;
    let obj = json_data.as_object().ok_or_else(|| {
        Error::AircraftDbError(format!(
            "Error parsing mictronics json file {}: not an object",
            json_path
        ))
    })?;
    for (i, (icao, data)) in obj.iter().enumerate() {
        if i > 0 && i % 100000 == 0 {
            println!("Inserted {} records", i);
        }
        let mut registration = data["r"].as_str().map(|s| s.to_string());
        if registration.is_some()
            && registration
                .as_ref()
                .expect("This shouldn't happen; registration is null")
                == ""
        {
            registration = None;
        }
        let mut ac_type = data["d"].as_str().map(|s| s.to_string());
        if ac_type.is_some()
            && ac_type
                .as_ref()
                .expect("This shouldn't happen; ac_type is null")
                == ""
        {
            ac_type = None;
        }
        db.execute(
            "INSERT INTO aircraft (icao, registration, type) VALUES (?1, ?2, ?3)",
            &[&Some(icao.to_uppercase()), &registration, &ac_type],
        )
        .map_err(|e| {
            Error::AircraftDbError(format!(
                "Error inserting into aircraft database {}; icao={}, reg={}, type={}: {}",
                &db_path,
                &icao,
                &registration.unwrap_or_else(|| "null".to_string()),
                &ac_type.unwrap_or_else(|| "null".to_string()),
                e
            ))
        })?;
    }
    db.execute_batch("CREATE UNIQUE INDEX idx_aircraft_icao ON aircraft (icao);")
        .map_err(|e| {
            Error::AircraftDbError(format!(
                "Error creating index in database {}: {}",
                &db_path, e
            ))
        })?;
    println!("Committing records...");
    db.execute_batch("COMMIT;").map_err(|e| {
        Error::AircraftDbError(format!("Error committing to database {}: {}", &db_path, e))
    })?;
    println!("Created {}", db_path);
    Ok(())
}
