use log::warn;
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, DurationMilliSeconds};
use std::{
    collections::HashMap,
    fs::File,
    io::{BufReader, Read},
    str::FromStr,
};

use crate::errors::Error;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct UrlConfig {
    pub url: String,
}

impl Default for UrlConfig {
    fn default() -> Self {
        UrlConfig {
            url: String::from(""),
        }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct ScreenshotConfig {
    pub attribution: Option<String>,
    pub layer: Option<String>,
    pub zoom: Option<u32>,
    pub simple_tracks: Option<bool>,
    pub dimensions: Option<(i32, i32)>,
}

impl Default for ScreenshotConfig {
    fn default() -> Self {
        ScreenshotConfig {
            attribution: None,
            layer: None,
            zoom: None,
            simple_tracks: None,
            dimensions: Some((800, 600)),
        }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct GeometryConfig {
    coordinates: (f64, f64),
}

impl Default for GeometryConfig {
    fn default() -> Self {
        GeometryConfig {
            coordinates: (0.0, 0.0),
        }
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Default)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct PerAirportConfig {
    geometry: GeometryConfig,
    properties: HashMap<String, String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Default)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct AirportsConfig {
    additional: Option<Vec<PerAirportConfig>>,
}

#[derive(Debug, Serialize, Deserialize, Default)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct AirportConfig {
    pub blocklist: Option<Vec<String>>,
    #[serde(skip)]
    pub blocklist_regexes: Vec<Regex>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Default)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct TwitterConfig {
    #[serde(rename = "enabled?")]
    pub is_enabled: bool,
    #[serde(skip)]
    pub creds: Option<TwitterCreds>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct LandmarksConfig {
    pub blocklist: Option<Vec<String>>,
    #[serde(skip)]
    pub blocklist_regexes: Vec<Regex>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct AdsbxConfig {
    pub url: String,
    #[serde(skip)]
    pub creds: Option<AdsbxCreds>,
}

impl Default for AdsbxConfig {
    fn default() -> Self {
        AdsbxConfig {
            url: String::from(""),
            creds: None,
        }
    }
}

#[serde_as]
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct AppConfig {
    pub adsbx: AdsbxConfig,
    pub pelias: UrlConfig,
    pub lat: Option<f64>,
    pub lon: Option<f64>,
    pub radius_km: Option<f64>,
    pub screenshot: Option<ScreenshotConfig>,
    pub landmarks: Option<LandmarksConfig>,
    pub icao_blocklist: Option<Vec<String>>,
    pub twitter: TwitterConfig,
    pub airport: Option<AirportConfig>,
    pub airports: Option<AirportsConfig>,
    pub icaos: Option<Vec<String>>,
    #[serde_as(as = "DurationMilliSeconds")]
    #[serde(rename = "max-history-age-ms")]
    #[serde(default = "default_max_history_age_ms")]
    pub max_history_age: std::time::Duration,
    pub rapid_api: Option<bool>,
    #[serde(default = "default_min_airport_distance_km")]
    pub min_airport_distance_km: f64,
    pub log_prefix: Option<String>,
    pub aircraft_info_db: Option<String>,
    pub simulate_circling_aircraft: Option<String>,
}

fn default_min_airport_distance_km() -> f64 {
    3.5
}

fn default_max_history_age_ms() -> std::time::Duration {
    std::time::Duration::from_secs(25 * 60)
}

impl Default for AppConfig {
    fn default() -> Self {
        AppConfig {
            adsbx: AdsbxConfig::default(),
            pelias: UrlConfig::default(),
            lat: None,
            lon: None,
            radius_km: None,
            screenshot: None,
            landmarks: None,
            icao_blocklist: None,
            twitter: TwitterConfig::default(),
            airport: None,
            airports: None,
            icaos: None,
            max_history_age: std::time::Duration::from_secs(25 * 60),
            rapid_api: None,
            min_airport_distance_km: 3.5,
            log_prefix: None,
            aircraft_info_db: None,
            simulate_circling_aircraft: None,
        }
    }
}

impl FromStr for AppConfig {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut config: AppConfig = serde_yaml::from_str(input)
            .map_err(|e| Error::ConfigError(format!("Error parsing config: {}", e)))?;
        // Lets eagerly compile the blocklist regexes so we detect errors right
        // away, instead of the rare time that we're about to tweet something.
        if let Some(ref mut airport) = config.airport {
            let blocklist = airport.blocklist.clone().unwrap_or_default();
            let regexes: Result<Vec<Regex>, _> = blocklist.iter().map(|s| Regex::new(s)).collect();
            airport.blocklist_regexes = regexes.map_err(|e| {
                Error::ConfigError(format!(
                    "Error parsing aircraft blocklist regex in config: {}",
                    e
                ))
            })?;
        }
        if let Some(ref mut landmarks) = config.landmarks {
            let blocklist = landmarks.blocklist.clone().unwrap_or_default();
            let regexes: Result<Vec<Regex>, _> = blocklist.iter().map(|s| Regex::new(s)).collect();
            landmarks.blocklist_regexes = regexes.map_err(|e| {
                Error::ConfigError(format!(
                    "Error parsing landmark blocklist regex in config: {}",
                    e
                ))
            })?;
        }
        if config
            .screenshot
            .as_ref()
            .and_then(|s| s.attribution.as_ref())
            .is_some()
        {
            warn!("The screenshot 'attribution' field is deprecated and has no effect.");
        }
        if config
            .screenshot
            .as_ref()
            .and_then(|s| s.simple_tracks.as_ref())
            .is_some()
        {
            warn!("The screenshot 'simple_tracks' field is deprecated and has no effect.");
        }
        Ok(config)
    }
}

impl AppConfig {
    pub fn from_file(filepath: &str) -> Result<AppConfig, Error> {
        let file = File::open(filepath).map_err(|e| {
            Error::ConfigError(format!("Error opening config file {}: {}", &filepath, e))
        })?;
        let mut buffered_reader = BufReader::new(file);
        let mut contents = String::new();
        buffered_reader.read_to_string(&mut contents).map_err(|e| {
            Error::ConfigError(format!("Error reading config file {}: {}", filepath, e))
        })?;
        AppConfig::from_str(&contents)
    }

    pub fn to_string(&self) -> Result<String, Error> {
        serde_yaml::to_string(self)
            .map_err(|e| Error::ConfigError(format!("Error serializing config: {}", e)))
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct AdsbxCreds {
    pub api_key: String,
    pub api_whitelist: Option<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct TwitterCreds {
    pub consumer_key: String,
    pub consumer_secret: String,
    pub access_token: String,
    pub access_token_secret: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case", deny_unknown_fields)]
pub struct Secrets {
    pub adsbx: Option<AdsbxCreds>,
    pub twitter: Option<TwitterCreds>,
}

impl Secrets {
    pub fn from_file(filepath: &str) -> Result<Secrets, Error> {
        let file = File::open(filepath).map_err(|e| {
            Error::ConfigError(format!("Error opening secrets file {}: {}", filepath, e))
        })?;
        let mut buffered_reader = BufReader::new(file);
        let mut contents = String::new();
        buffered_reader.read_to_string(&mut contents).map_err(|e| {
            Error::ConfigError(format!("Error reading secrets file {}: {}", filepath, e))
        })?;
        Secrets::from_str(&contents)
            .map_err(|e| Error::ConfigError(format!("Error parsing secrets: {}", e)))
    }

    pub fn to_string(&self) -> Result<String, Error> {
        serde_yaml::to_string(self)
            .map_err(|e| Error::ConfigError(format!("Error serializing secrets: {}", e)))
    }
}

impl FromStr for Secrets {
    type Err = serde_yaml::Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let creds: Secrets = serde_yaml::from_str(input)?;
        Ok(creds)
    }
}

#[cfg(test)]
mod tests {
    #![allow(clippy::unwrap_used)]

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_config_from_str() {
        let c = r#"
          adsbx:
            # url: https://adsbexchange.com/api/aircraft/json
            url: http://localhost:8000/api/aircraft/json
          pelias:
            url: http://lockheed.local:4000/v1
          lat: 34.1335
          lon: -118.1924
          radius-km: 50
          max-history-age-ms: 3600000
          screenshot:
            attribution: © OpenStreetMap contributors
          landmarks:
            blocklist:
              - Johnny.Depp
          icao-blocklist:
            - ADF7F9
          twitter:
            enabled?: true
          airport:
            blocklist:
              - Puente Sky Ranch
              - Radio Control
        "#;
        let config = AppConfig::from_str(c).unwrap();
        // Simple check of parsing.
        assert_eq!(config.max_history_age, std::time::Duration::new(3600, 0));
    }

    #[test]
    fn test_creds_from_str() {
        let c = r#"
          adsbx:
            api-key: 123
            api-whitelist: 456
          twitter:
            consumer-key: 789
            consumer-secret: 012
            access-token: 345
            access-token-secret: 678
        "#;
        let creds = Secrets::from_str(c).unwrap();
        assert_eq!(creds.adsbx.as_ref().unwrap().api_key, "123".to_string());
        assert_eq!(
            Secrets::from_str(&creds.to_string().unwrap()).unwrap(),
            creds
        );
    }
}
