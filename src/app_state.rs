use std::collections::HashMap;
use std::fs::File;
use std::io::{BufReader, Read, Write};
use std::path::Path;
use std::str::FromStr;

use log::warn;
use serde::{Deserialize, Serialize};

use crate::aircraft::AircraftState;
use crate::errors::Error;

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct AppState(pub HashMap<String, AircraftState>);

impl AppState {
    pub fn from_file(filepath: &str) -> Result<AppState, Error> {
        let file = File::open(filepath);
        match file {
            Ok(file) => {
                let mut buffered_reader = BufReader::new(file);
                let mut contents = String::new();
                buffered_reader.read_to_string(&mut contents).map_err(|e| {
                    Error::AppStateError(format!(
                        "Error reading app state from {}: {}",
                        filepath, e
                    ))
                })?;
                AppState::from_str(&contents).map_err(|e| {
                    Error::AppStateError(format!(
                        "Error parsing app state from {}: {}",
                        filepath, e
                    ))
                })
            }
            Err(err) => match err.kind() {
                std::io::ErrorKind::NotFound => {
                    warn!(
                        "History file {} does not exist; using empty history",
                        filepath
                    );
                    Ok(AppState(HashMap::new()))
                }
                _ => Err(Error::AppStateError(format!(
                    "Error opening history file {}: {}",
                    filepath, err
                ))),
            },
        }
    }

    pub fn save_file(&self, filepath: &str) -> Result<File, Error> {
        let mut tmpfile = match Path::new(filepath).parent() {
            Some(dir) => tempfile::NamedTempFile::new_in(dir).map_err(|e| {
                Error::AppStateError(format!(
                    "Error creating temp file in directory {}: {}",
                    dir.display(),
                    e
                ))
            })?,
            None => tempfile::NamedTempFile::new_in(".")
                .map_err(|e| Error::AppStateError(format!("Error creating temp file: {}", e)))?,
        };
        match tmpfile.write_all(self.to_string()?.as_bytes()) {
            Ok(_) => tmpfile.persist(filepath).map_err(|err| {
                Error::AppStateError(format!("Error persisting temp file {}: {}", filepath, err))
            }),
            Err(err) => Err(Error::AppStateError(format!(
                "Error writing temp file {}: {}",
                filepath, err
            ))),
        }
    }

    fn to_string(&self) -> Result<String, Error> {
        serde_json::to_string(self)
            .map_err(|e| Error::AppStateError(format!("Error serializing app state: {}", e)))
    }
}

impl FromStr for AppState {
    type Err = serde_json::Error;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let history: AppState = serde_json::from_str(input)?;
        Ok(history)
    }
}

#[cfg(test)]
mod tests {
    #![allow(clippy::unwrap_used)]

    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_read_ac_state_file() {
        let json_str = r#"
            {
                "~318545": {
                    "military?": false,
                    "alt": 0,
                    "speed": 11.2,
                    "normalized-curviness": 0,
                    "squawk": null,
                    "icao": "~318545",
                    "geom-alt": 55,
                    "history": [
                      {
                          "lat": 33.942342,
                          "lon": -118.430657,
                          "alt": 0,
                          "gnd?": true,
                          "time": 1616263829683
                        }
                    ],
                    "curviness": 0,
                    "gnd?": true,
                    "callsign": "VIPER1",
                    "lon": -118.430657,
                    "lat": 33.942435,
                    "registration": null,
                    "postime": "2021-12-07T07:21:02.473018Z"
                }
            }
        "#;
        let hist = AppState::from_str(json_str).unwrap();
        assert_eq!(hist.0.len(), 1);
        assert_eq!(hist.0["~318545"].call_sign.as_ref().unwrap(), "VIPER1");
        assert_eq!(
            AppState::from_str(&hist.to_string().unwrap()).unwrap(),
            hist
        );
    }
}
