use thiserror::Error;

/// The adsbx_browser error type.
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    /// Error related to configuration data.
    #[error("{0}")]
    ConfigError(String),

    /// Error related to app state data.
    #[error("{0}")]
    AppStateError(String),

    /// Error tweeting.
    #[error("{0}")]
    TweetError(String),

    /// GeoJSON error.
    #[error("{0}")]
    GeoJsonError(String),

    /// Pelias error.
    #[error("{0}")]
    PeliasError(String),

    /// Aircraft Database error.
    #[error("{0}")]
    AircraftDbError(String),

    /// ADS-B Exchange API error.
    #[error("{0}")]
    AdsbxApiError(String),

    /// ADS-B Exchange screenshot error.
    #[error("{0}")]
    AdsbxScreenshotError(String),

    /// Aircraft photo error.
    #[error("{0}")]
    AircraftPhotoError(String),

    /// Aircraft state error.
    #[error("{0}")]
    AircraftStateError(String),

    /// Wildfire data error.
    #[error("{0}")]
    WildfireError(String),
}
