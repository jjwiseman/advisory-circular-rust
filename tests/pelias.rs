use advisorycircular::pelias::*;
use geo::Point;

#[tokio::test]
#[ignore]
// To run this, do `cargo test -- --include-ignored`.
async fn test_nearby() {
    let pelias = PeliasApi::from_url("http://lockheed.local:4000").unwrap();
    let r = pelias
        .nearby(&NearbyOptions {
            point: Some(Point::new(-118.3025822, 34.1184341)),
            boundary: Some(Boundary::Circle(100.0)),
            layers: Some("venue"),
            size: Some(5),
            ..NearbyOptions::default()
        })
        .await
        .unwrap();
    println!("{:#?}", r);
    let labels: Vec<String> = r
        .features
        .iter()
        .map(|f| f.properties.label.clone())
        .collect();
    assert_eq!(
        labels,
        vec![
            "Gottlieb Transit Corridor, Los Angeles, CA, USA".to_string(),
            "James Dean, Los Angeles, CA, USA".to_string(),
            "Griffith Observatory, Los Angeles, CA, USA".to_string(),
            "Solar System, Los Angeles, CA, USA".to_string(),
            "Astronomers\' Monument, Los Angeles, CA, USA".to_string(),
        ]
    );
}
